module.exports = {

  /**
   * Headers of the page
   */
  head: {
    titleTemplate: '%s - LTB Web',
  },

  manifest: {
    name: 'London Transit Buses (Lite)',
    short_name: 'LT Buses (Lite)',
    display: 'standalone',
    background_color: "#0073c6",
    start_url: "/times",
  },

  /**
   * Customize the progress bar color
   */
  loading: {color: '#0073c6'},
  
  css: [
    `@/assets/main.scss`,
  ],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
  ],

  /**
   * Build configuration
   */
  build: {
    /**
     * Run ESLint on save
     */
    extend (config, {isDev, isClient}) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
    },
  },
};